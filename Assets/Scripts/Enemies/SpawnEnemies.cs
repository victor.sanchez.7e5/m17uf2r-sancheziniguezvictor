using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;

public class SpawnEnemies : Enemy
{
    [SerializeField]
    public GameObject enemy;
    public GameObject [] spawnerEnemies = new GameObject[5];
    public int randomSpawner1;
    public int randomSpawner2;

    float next_spawn_time;

    //NEXT SPAWNER TURRETS

    private int minionCounter = 0;
    public static event Action eventNextEnemies = delegate { };

    void Start()
    {
        randomSpawner1 = RandomSpawnerGeneratedVALUE();
        randomSpawner2 = RandomSpawnerGeneratedVALUE2();

        for (int i = 0; i < spawnerEnemies.Length; i++)
        {
            spawnerEnemies[i].SetActive(false);
        }
        spawnerEnemies[randomSpawner1].SetActive(true);
        spawnerEnemies[randomSpawner2].SetActive(true); 

        next_spawn_time = Time.time + 1.5f;
        
        
    }


    // EVENTO 
    //public void OnEnable()
    //{
    //    Hunter.eventHunterDie += CanSpawnHunter;
    //    SantaClausBoss.BossSpawned += SpawnHuntersAgain;
    //}
    //private void OnDisable()
    //{
    //    Hunter.eventHunterDie -= CanSpawnHunter;
    //    SantaClausBoss.BossSpawned -= SpawnHuntersAgain;
    //}
    //public void CanSpawnHunter()
    //{
    //    huntervivo = false;
    //}
    //public void SpawnHuntersAgain() { santavivo = false; }


    void Update()
    {
       
        if (Time.time > next_spawn_time)
        {
            //Instantiate enemy clones prefab...
            Spawn();

            next_spawn_time += 5.0f;
        }

        if(minionCounter >= 10)
        {
            //Event per comen�ar l'spawn de turrets
            eventNextEnemies.Invoke();
            //int randomIndexPrefSpawner = RandomSpawnerGeneratedVALUE2();
            //do
            //{
            //    RandomSpawnerGeneratedVALUE2();
            //} while (randomSpawner1 == randomIndexPrefSpawner || randomSpawner2 == randomIndexPrefSpawner);
            //spawnerEnemies[randomIndexPrefSpawner].SetActive(true);

            //Debug.Log(minionCounter);
            

        }

    }


    private void Spawn()
    {
        Instantiate(enemy, this.gameObject.transform.position, transform.rotation);
        //Instantiate(enemy, this.gameObject.transform.position, transform.rotation);
        //No funciona correcte perque cada spawner instancia 2 a l'hora el seu i el de l'altre. 
        minionCounter++;
    }
}
