using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private new Rigidbody2D rigidbody;
    public Animator animatorBullet;

    public float speed;
    //private int balasTotales = 15; 

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animatorBullet.SetBool("Explota", false);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("enemy") || collision.CompareTag("wall") )
        {
            animatorBullet.SetBool("Explota", true);
            speed = /*-0.5f*/0f;
            //StartCoroutine(DestroyAfterAnim());
            Destroy(gameObject,0.30f);
        }
        
    }

    //IEnumerator DestroyAfterAnim()
    //{

    //    yield return new WaitForSeconds(1);

    //}

    void FixedUpdate()
    {
        rigidbody.MovePosition(transform.position + transform.right * speed * Time.fixedDeltaTime);
    }

    
}
