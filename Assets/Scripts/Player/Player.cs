using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class Player : Character
{
    public static event Action playerDeathEvent = delegate { };

    public static int coins;
    public static int vida;

    public Character CharacterData;

    public int danyStandardBullet = 20;     //Mes endevant cada arma treura diferents quantitats de dany 


    void Start()
    {
        Name = "Warrior";
        Vida = 100;
        coins = 0; 
        Speed = 7.5f;

        CharacterData.Vida = 100;       //error en esta linia

        vida = CharacterData.Vida;
        coins = CharacterData.coins;
    }

    void Update()
    {

        if (vida <= 0)
        {
            anim.SetBool("isPlayerDeath", true);
            anim.SetFloat("Blend_X", 0);
            anim.SetFloat("Blend_Y", 0);

            FindObjectOfType<AudioManager>().Play("Death");
            Debug.Log("Me muero");
            StartCoroutine(WaitAndDie());
        }
    }
    IEnumerator WaitAndDie()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("GameOver");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.collider.tag == "enemy")
        {
            anim.SetBool("hit", true);
            StartCoroutine(HitRecovery());

            vida -= danyStandardBullet;
            FindObjectOfType<AudioManager>().Play("Playerhit");
        }
        if (collision.collider.tag == "cora")
        {
            vida += 20;
            Destroy(collision.gameObject);
        }

    }

    IEnumerator HitRecovery()
    {
        yield return new WaitForSeconds(0.25f);
        anim.SetBool("hit", false);
    }

    private void OnDestroy()
    {
        playerDeathEvent.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("coins"))
        {
            coins += 1;
            Destroy(collision.gameObject);
        }
    }

    public void TakeDamage() { }    //Implementar interficie IDamageble
                                    //public void DestroyItems()
                                    //{
                                    //    GameObject ammoBOX = GameObject.Find("ammobox(Clone)");
                                    //    GameObject corazonVida = GameObject.Find("corazonVida(Clone)");
                                    //    Destroy(ammoBOX);
                                    //    Destroy(corazonVida);
                                    //}


}
