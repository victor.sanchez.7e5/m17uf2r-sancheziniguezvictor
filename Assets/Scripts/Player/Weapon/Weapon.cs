using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Weapon : MonoBehaviour
{
    public static Weapon instance;

    public new Camera camera;
    public Transform spawnerBales;
    public GameObject bulletPrefab;

    public int balesQueden;
    public int balesTotals;
    public Text bulletCounter;

    //public GameObject currentWeaponPrefab;

    public SpriteRenderer itemSprite;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("M�s de una instancia de arma");
            return;
        }
        instance = this;
    }

    void Start()
    {
        itemSprite = GetComponent<SpriteRenderer>();
        balesQueden = 30;
        balesTotals = 30;
        
    }

    void Update()
    {
        RotateTowardsMouse();
        CheckFiring();
    }

    private void RotateTowardsMouse()
    {
        float angle = GetAngleTowardsMouse();

        transform.rotation = Quaternion.Euler(0, 0, angle);
        //spriteRenderer.flipY = angle >= 90 && angle <= 270;
    }

    private float GetAngleTowardsMouse()
    {
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(Input.mousePosition);

        Vector3 mouseDirection = mouseWorldPosition - transform.position;
        mouseDirection.z = 0;

        float angle = (Vector3.SignedAngle(Vector3.right, mouseDirection, Vector3.forward) + 360) % 360;

        return angle;
    }

    private void CheckFiring()
    {
        if (Input.GetMouseButtonDown(0) && balesQueden > 0)
        {
            GameObject bullet = Instantiate(bulletPrefab);

            balesQueden--;
            Debug.Log(balesQueden + "/" + balesTotals);
            bulletCounter.text = balesQueden + "/" + balesTotals + " Bullets";

            bullet.transform.position = spawnerBales.position;
            bullet.transform.rotation = transform.rotation;
            Destroy(bullet, 2f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ammobox"))
        {
            balesQueden += 5;
            Destroy(collision.gameObject);
        }
    }

    public void EquipWeapon(ItemData item)
{
    if (item is WeaponData weaponItem && weaponItem.icon != null)
    {
        itemSprite.sprite = weaponItem.icon;

        if (bulletPrefab != null)
        {
            spawnerBales = bulletPrefab.transform;
        }

        balesQueden = weaponItem.BulletsCarregador;
        balesTotals = weaponItem.BulletsTotal;
        bulletCounter.text = balesQueden + "/" + balesTotals + " Bullets";
    }
}
}
