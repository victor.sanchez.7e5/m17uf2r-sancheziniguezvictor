using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public ItemData item;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) PickUp();
        
    }

    void PickUp()
    {
        Debug.Log("Picking up");
        //Add To inventory
        Destroy(this.gameObject);
        Inventory.instance.Add(item);
    }
}
