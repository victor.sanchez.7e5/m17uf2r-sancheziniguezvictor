using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Item", menuName = "Inventory/Item")]
public class ItemData : ScriptableObject
{
    new public string name = "New Item";
    public GameObject itemPrefab;
    public Sprite icon = null;
    public bool isDefaultItem = false;

    public virtual void Use()
    {

    }
}
