using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Character : MonoBehaviour
{

    public string Name;
    public int Vida;
    public int coins;
    public float Speed;
    public Animator anim;

    public void AnimatePositions (float charPositionX, float charPositionY)
    {
        
        anim.SetFloat("Blend_X", charPositionX);
        anim.SetFloat("Blend_Y", charPositionY);
        
    }
}
