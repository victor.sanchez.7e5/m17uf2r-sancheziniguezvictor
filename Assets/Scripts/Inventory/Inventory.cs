using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory instance; 
    
    public List<ItemData> items = new List<ItemData>();

    public int slotsNum = 5;

    public delegate void OnItemChanged();

    public OnItemChanged onItemChangedCallback; 

    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("M�s de una instancia d'inventari");
            return;
        }
        instance = this;
    }

    public bool Add(ItemData item)
    {
        if (!item.isDefaultItem)
        {

            if (items.Count >= slotsNum)
            {
                Debug.Log("No queda espai");
                return false;
            }

            items.Add(item);

            if(onItemChangedCallback!= null) onItemChangedCallback.Invoke(); 
        }

        return true;
    }

    public void Remove(ItemData item)
    {
        items.Remove(item);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SelectItemAtIndex(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SelectItemAtIndex(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SelectItemAtIndex(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SelectItemAtIndex(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            SelectItemAtIndex(4);
        }
    }

    private void SelectItemAtIndex(int index)
    {
        if (index < 0 || index >= items.Count)
        {
            Debug.Log("No hay un �tem en ese �ndice.");
            return;
        }

        ItemData selectedItem = items[index];
        FindObjectOfType<Weapon>().EquipWeapon(items[index]);
        // Realiza las acciones necesarias con el �tem seleccionado
        Debug.Log("�tem seleccionado: " + selectedItem.name);
    }
}
