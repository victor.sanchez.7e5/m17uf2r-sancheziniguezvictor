using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class PlayerWeapon : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    public new Camera camera;
    public Transform spawner;
    public GameObject bulletPrefab;

    //Numero actual de bales en el carregador
    public int balesQueden;
    //Numero total de bales que teniem 
    public int balesTotals;
    public Text bulletCounter;

    //Escribe un script para unity 2d que dirija una arma hacia el puntero de raton pero con unos angulos definidos como limite de movimiento del arma

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        balesQueden = 30;
        balesTotals = 30;
    }

    void Update()
    {
        RotateTowardsMouse();
        CheckFiring();
    }

    private void RotateTowardsMouse()
    {
        float angle = GetAngleTowardsMouse();

        transform.rotation = Quaternion.Euler(0, 0, angle);
        //spriteRenderer.flipY = angle >= 90 && angle <= 270;
    }

    private float GetAngleTowardsMouse()
    {
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(Input.mousePosition);

        Vector3 mouseDirection = mouseWorldPosition - transform.position;
        mouseDirection.z = 0;

        float angle = (Vector3.SignedAngle(Vector3.right, mouseDirection, Vector3.forward) + 360) % 360;

        return angle;
    }

    private void CheckFiring()
    {
        if (Input.GetMouseButtonDown(0) && balesQueden > 0)
        {
            GameObject bullet = Instantiate(bulletPrefab);

            //Cada cop que instanciem una bala restem 1 al carregador
            //UI
            balesQueden--;
            Debug.Log(balesQueden + "/" + balesTotals);
            bulletCounter.GetComponent<Text>().text = balesQueden + "/" + balesTotals + "Bullets";

            bullet.transform.position = spawner.position;
            bullet.transform.rotation = transform.rotation;
            Destroy(bullet, 2f);

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ammobox") ) 
        {
            balesQueden += 5;
            Destroy(collision.gameObject);
        }    
    }

}
