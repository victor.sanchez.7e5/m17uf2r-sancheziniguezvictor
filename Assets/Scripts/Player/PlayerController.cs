using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerController : Character
{
    public Rigidbody2D playerRigidbody2D;

    void Update()
    {

        PlayerMovement(); 

    }

    public void PlayerMovement()
    {
        float playerPositionX = Input.GetAxis("Horizontal");
        float playerPositionY = Input.GetAxis("Vertical");

        AnimatePositions(playerPositionX, playerPositionY);
        
        Vector2 movement = new Vector2(playerPositionX * Speed, playerPositionY * Speed);

        playerRigidbody2D.velocity = movement;
    }

}

