using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class ShopMenu : MonoBehaviour
{
    public static bool isShopOpen = false;

    public GameObject shopMenuUI; 

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))  //Al fer clic TAB obre la shop 
        {
            if (isShopOpen) Resume();
            else OpenShop();
        }
    }

    public void Resume()
    {
        shopMenuUI.SetActive(false);
        Time.timeScale = 1;
        isShopOpen = false; 
    }

    public void OpenShop()
    {
        shopMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isShopOpen = true; 
    }

}
