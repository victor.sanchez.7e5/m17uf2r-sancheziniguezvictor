using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    
    private GameObject player;   //volem aconseguir la position del player (en el transform)
    private Transform playerTransform;

    private Rigidbody2D enemyRb;
    private Vector2 enemyMovement;

    //Control de Items que dropeja
    bool otherItemOnScene;
    public GameObject coinPref; 
    public GameObject heartLife;
    public GameObject ammoBox;

    public Animator enemyAnim;

    void Start()
    {
        Name = "Creeper";
        Vida = 15;  
        Speed = 3f;

        player = GameObject.Find("Player");
        playerTransform = player.transform; 
        enemyRb = this.GetComponent<Rigidbody2D>();

        //En comen�ar l'escena no hi ha cap item a la partida 
        otherItemOnScene = false;
        next_spawn_timeEn = Time.time + 1.5f; 
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "arma")
        {
            Vida -= 5;
            FindObjectOfType<AudioManager>().Play("Enemyhit");
            //Explosion animation; 
            otherItemOnScene = false; 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {
            Vida -= 5;
            FindObjectOfType<AudioManager>().Play("Enemyhit");
        }
    }


    float next_spawn_timeEn;
    void Update()
    {
        EnemyFollowingPlayer();

        if (Time.time > next_spawn_timeEn)
        {
            //Instantiate enemy clones prefab...
            StartCoroutine(DropCoins());

            next_spawn_timeEn += 5.0f;
        }

        EnemyTakingDamage(); 
            
    }

    public void EnemyFollowingPlayer()
    {
        Vector3 direction = playerTransform.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        direction.Normalize();
        enemyMovement = direction;
        //Debug.Log(direction);
    }

    /// <summary>
    ///Quan Vida arribi a 0 es destrueix el prefab
    ///Falta per implementar animaci� 
    /// </summary>
    void EnemyTakingDamage()
    {
        if (Vida <= 0)
        {
            DroppingItems();
            //enemyAnim.SetBool("IsDeath", true);
            //StartCoroutine(Destroyyy()); 
            Destroy(gameObject);

        }
    }


    /// <summary>
    /// Asociem al Fixed Update el moviment del enemic al del Player
    /// </summary>
    private void FixedUpdate()
    {
        moveCharacter(enemyMovement); 
    }

    void moveCharacter (Vector2 direction)
    {
        enemyRb.MovePosition((Vector2) transform.position + (direction * Speed * Time.deltaTime));      //Null Reference
    }

    /// <summary>
    /// Numeros de Spawners de l'array Random
    /// </summary>
    /// <returns></returns>
    public int RandomSpawnerGeneratedVALUE()
    {
        int randomPref = Random.Range(0 , 5);
        return randomPref; 
    }

    public int RandomSpawnerGeneratedVALUE2()
    {
        int randomPref = 0;
        do
        {
            randomPref = Random.Range(0, 5);
        } while (randomPref == RandomSpawnerGeneratedVALUE()); 

        return randomPref;
    }

    /// <summary>
    /// En morir s'activa aquesta funci� que controla la probabilitat de quin item deixar� l'enemic en morir : 
    /// </summary>
    void DroppingItems()
    {
        float randomValue = Random.Range(0f, 1f);

        if (randomValue <= 0.6f && !otherItemOnScene)
        {
            Instantiate(ammoBox, enemyRb.transform.position, transform.rotation);
            otherItemOnScene = true;
        }

        if (randomValue <= 0.7 && !otherItemOnScene)
        {
            Instantiate(heartLife, enemyRb.transform.position, transform.rotation);
            otherItemOnScene = true;
        }
    }

    /// <summary>
    /// Corutina per tirar monedes
    /// </summary>
    /// <returns></returns>
    IEnumerator DropCoins()
    {
        Instantiate(coinPref, enemyRb.transform.position, transform.rotation);
        yield return new WaitForSeconds(2);
    }

}
