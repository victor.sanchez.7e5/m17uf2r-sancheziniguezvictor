using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{

    /// <summary>
    /// On clic PLAY AGAIN 
    /// </summary>
    public void BackToGame()
    {
        SceneManager.LoadScene("GameScene"); 
    }
    
    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");

    }

    public void ExitGame()
    {
        Application.Quit(); 
    }
}

