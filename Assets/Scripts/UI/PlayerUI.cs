using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : Player
{
    public Text bulletCounter;

    public Slider barraVida;
    private GameObject hitPoints;

    private GameObject numCoins;

    public void Start()
    {
        hitPoints = GameObject.Find("HP");
        numCoins = GameObject.Find("NumCoins");

        barraVida.value = vida;
        hitPoints.GetComponent<Text>().text = vida + " HP";        
        numCoins.GetComponent<Text>().text = coins + " $";        

    }
    private void Update()
    {
        CoinsCounter();

        BarraVida();
    }

    public void BarraVida()
    {
        barraVida.value = vida;
        hitPoints.GetComponent<Text>().text = vida + " HP";        //Si treiem aix� durant l'animacio de morir la vida arriba a ser en numeros negatiu

    }

    public void CoinsCounter()
    {
        numCoins.GetComponent<Text>().text = coins + " $";        //Si treiem aix� durant l'animacio de morir la vida arriba a ser en numeros negatiu
    }

}