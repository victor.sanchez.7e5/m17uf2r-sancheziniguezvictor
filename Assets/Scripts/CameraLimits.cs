using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimits : MonoBehaviour
{
    //public Bounds targetBounds;

    //private Camera cam;
    public GameObject player;
    public Vector3 posicion; 

    void Start()
    {
        posicion = transform.position - player.transform.position; 

    }

    void Update()
    {
        /* 
         * Arriba izq limit --> -11.54, 8.49
         * Arriba derch limit --> 12.29 , 8.49
         * Abajo izq limit -->-11.54 , -1.56
         * Abajo derch -->     12.29, -1.65 
         */
        transform.position = player.transform.position + posicion;
    }

    
    
    
}
